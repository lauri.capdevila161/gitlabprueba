
package gitlabtest;

public class Main {
     public static void main(String[] args) {
        Persona p1 = new Persona();
        p1.nombre = "Laura";
        p1.apellido = "Capdevila";
        
        Persona p2 = new Persona();
        p2.nombre = "Elisa";
        p2.apellido = "Castro";
        
        p1.infoDesplegar();
        System.out.println("Persona 2");
        p2.infoDesplegar();
    }
}
